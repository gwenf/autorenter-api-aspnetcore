using System;
using System.IO;
using AutoRenter.Domain.Data;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace AutoRenter.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var webHost = BuildWebHost(args);

            InitializeDB(webHost);

            webHost.Run();
        }

        private static IWebHost BuildWebHost(string[] args)
        {
            var envUrls = Environment.GetEnvironmentVariable("ASPNETCORE_URLS");
            if (!string.IsNullOrWhiteSpace(envUrls))
            {
                return BuildHostWithUrls(args, envUrls);
            }

            if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Development")
            {
                return BuildHostWithUrls(args, "http://*:3000");
            }

            return BuildHost(args);
        }

        private static IWebHost BuildHostWithUrls(String[] args, string urls)
        {
            return WebHost.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.AddEnvironmentVariables("ASPNETCORE_");
                })
                .UseStartup<Startup>()
                .UseUrls(urls)
                .Build();
        }

        private static IWebHost BuildHost(String[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.AddEnvironmentVariables("ASPNETCORE_");
                })
                .UseStartup<Startup>()
                .Build();
        }

        private static void InitializeDB(IWebHost webHost)
        {
            using (var scope = webHost.Services.GetService<IServiceScopeFactory>().CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    using (var context = services.GetRequiredService<AutoRenterContext>())
                    {
                        AutoRenterDbInitializer.Initialize(context);
                    }
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred while seeding the database.");
                }
            }
        }
    }
}
