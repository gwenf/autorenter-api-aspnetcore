﻿using Microsoft.AspNetCore.Mvc;
using AutoRenter.Domain.Models;

namespace AutoRenter.Api.Services
{
    public interface IResultCodeConverter
    {
        IActionResult Convert(ResultCode resultCode);
    }
}
